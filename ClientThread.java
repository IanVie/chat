import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

import javax.swing.JOptionPane;
class clientThread extends Thread {

	private String clientName;
	private DataInputStream is;
	private PrintStream os;
	private Socket clientSocket;
	private final clientThread[] threads;
	private int maxClientsCount;
	private boolean online = true;
	private boolean admin;

	public clientThread(Socket clientSocket, clientThread[] threads, int users) {
		this.clientSocket = clientSocket;
		this.threads = threads;
		maxClientsCount = threads.length;
	}


	public void run() {
		int maxClientsCount = this.maxClientsCount;
		clientThread[] threads = this.threads;

		try {

			is = new DataInputStream(clientSocket.getInputStream());
			os = new PrintStream(clientSocket.getOutputStream());
			String name;
			name = is.readLine().trim();
			os.println("***Welcome " + name + " to our chat room.***");


			synchronized (this) 
			{
				for (int i = 0; i < maxClientsCount; i++) 
				{
					if (threads[i] != null && threads[i] == this) 
					{
						clientName = name;
						break;
					}

					if(i==0)
					{
						threads[0].admin=true;
					}
				}
				for (int i = 0; i < maxClientsCount; i++) 
				{
					if (threads[i] != null && threads[i] != this) 
					{
						threads[i].os.println("*** A new user " + name
								+ " entered the chat room !!! ***");
					}
				}

			}


			/* Start the conversation. */
			while (true) {
				String line = is.readLine();

				if (line.startsWith("/offline")) 
				{
					for (int i = 0; i < maxClientsCount; i++) 
					{
						if(threads[i] == this)
						{
							threads[i].online=false;
							os.println(threads[i].clientName + " you now offline to re-enter" + "press /online");
						}
					}
				}
				else if (line.startsWith("/online")) 
				{
					for (int i = 0; i < maxClientsCount; i++) 
					{
						if(threads[i] == this){
							threads[i].online = true;
							os.println("Welcome back " + threads[i].clientName);
						}
					}
				}				
				else if (line.startsWith("/quit")) 
				{
					os.println("**Bye**");
					for (int i = 0; i < maxClientsCount; i++) 
					{
						if (threads[i] == this) {
							threads[i] = null;
						}
					}

					System.exit(0);
					break;
				}
				else if (line.startsWith("/users")) 
				{
					os.println("***users***");
					for (int j = 0; j < 10; j++) 
					{
						if(threads[j]!=null)
							os.println(1+j+" : " + threads[j].clientName);
					}
					os.println("*** ***");
				}


				if(line.startsWith("/help"))
				{
					JOptionPane.showMessageDialog(null,
							"/help  :  list of all commands"+'\n'+
							"/users :  list of all users"+'\n'+
							"/quit  : leave the chat"+'\n'+
							"/offline : go on busy mode"+'\n'+
							"/online :  come back on the online"+'\n'+ 
							"/block : block other user"+'\n'+
							"/changename : change ur user name ");
				}

				if(line.startsWith("/changename"))
				{
					this.clientName = JOptionPane.showInputDialog("Enter Your Name");
					os.println("Your new name is" + name);
				}

				if(line.startsWith("/block"))
				{
					int off;
					if(this.admin){
						os.println("***users***");
						for (int j = 0; j < 10; j++) 
						{
							if(threads[j]!=null){
								os.println(1+j+" : " + threads[j].clientName);
							}
						}
						os.println("*** ***");
						off = Integer.parseInt(JOptionPane.showInputDialog("Enter The number next to the user"));
						threads[off-1].os.println("you been blocked you cant participate anymore");
						threads[off-1] = null;
					}
					else
					{
						os.println("You dont have admin rights");
					}
				}

				if (line.contains(":")&& line.length()>0&& !line.startsWith("/") ) 
				{
					String to = line.substring(0, line.indexOf(":")).trim();
					line = line.substring(line.indexOf(":") + 2, line.length());
					synchronized (this) 
					{
						for (int i = 0; i < maxClientsCount; i++) 
						{
							if (threads[i] != null && threads[i] != this
									&& threads[i].clientName != null
									&& threads[i].clientName.equals(to)) 
							{
								threads[i].os.println("<" + name + "> " + line);
								this.os.println("<me to " + to + " > " + line);
								break;
							}
							else if (i==9)
							{
								os.println("The user does not exist \n");


								os.println("***users***");
								for (int j = 0; j < 10; j++) 
								{
									if(threads[j]!=null)
										os.println(1+j+" : " + threads[j].clientName);
								}
								os.println("*** ***");
							}
						}
					}       
				}
				else if(line.length()>0 && !line.startsWith("/")) 
				{
					synchronized (this) 
					{
						for (int i = 0; i < maxClientsCount; i++) 
						{
							if (threads[i] != null && threads[i].clientName != null && threads[i].online) 
							{
								if (threads[i] == this)
								{
									threads[i].os.println("me: " + line);
								}
								else
								{
									threads[i].os.println(name + ": " + line);
								}
							}
						}
					}
				}
			}

			is.close();
			os.close();
			clientSocket.close();

		}
		catch (IOException e) 
		{
			System.out.println("Its an empty catch");
		}

	}
}
