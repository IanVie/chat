
import java.io.DataInputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;

/*
 * A chat server that delivers public and private messages.
 */
public class Server {


	private static ServerSocket serverSocket;// = null;
	private static Socket clientSocket;// = null;
	private static final int maxClientsCount = 10;
	private static final clientThread[] threads = new clientThread[maxClientsCount];

	public static void main(String args[]) throws IOException {


		int portNumber = 8001;
		System.out.print("The server is running");
		serverSocket = new ServerSocket(portNumber);
		while (true) {

			clientSocket = serverSocket.accept();
			
			for (int i = 0; i < maxClientsCount; i++) {
				if (threads[i] == null) {
					(threads[i] = new clientThread(clientSocket, threads,i)).start();
					break;
				}
			}
	
		}
	}
}