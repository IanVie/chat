import java.io.DataInputStream;
import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

import javax.swing.JOptionPane;

public class Client implements Runnable {

	private static Socket clientSocket;
	private static PrintStream os;
	private static DataInputStream is;
	private static boolean closed;
	private static ChatPanel gui;
	//private static String [] AllNames;
	
	 public static boolean nameCheck(String name){
		 int k;
		 for(int i =0; i<name.length();i++)
		 {
			 k =(int)name.charAt(i);
			 if(!((k>=97 && k<=122) || (k>=65 && k<=90))){
				 return false;
			 }
		 }
		 
		 if(name.length()<2)
		 {
			 return false;
		 }
		 
		return true; 
	 } 

	@SuppressWarnings("unused")
	public static void main(String[] args) throws InterruptedException, UnknownHostException, IOException {

		gui = new ChatPanel();
		int portNumber = 8001;
		String host = "localhost";
		System.out.println("Client running");
		clientSocket = new Socket(host, portNumber);
		os = new PrintStream(clientSocket.getOutputStream());
		is = new DataInputStream(clientSocket.getInputStream());

		while (true){
			String name = JOptionPane.showInputDialog("Enter Your Name");
			gui.updateINF("You are now online");
			if(nameCheck(name)){
				os.println(name);
				break;
			}
			else{
				JOptionPane.showMessageDialog(null,
						"Your name should contain alpha characters only, "
								+ "it must have at least three letters  ",
								"Inane error",
								JOptionPane.ERROR_MESSAGE);
			}
		}

		new Thread(new Client()).start();
		while (!closed) {
			Thread.sleep(1);
			if (gui.sent == true){
				os.println(gui.message);
				gui.sent = false;
			}
		}
		os.close();
		is.close();
		clientSocket.close();
	}

	public void run() {

		String responseLine;
		try {
			while ((responseLine = is.readLine()) != null) {
				if (responseLine.startsWith("<")){
					gui.appendPM(responseLine);
				}else{
					gui.appendGM(responseLine);
				}

			}
			closed = true;
		} catch (IOException e) {
			System.err.println("IOException:  " + e);
		}
	}
}
