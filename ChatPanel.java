import javax.swing.*;
import javax.swing.border.TitledBorder;

import java.awt.*;
import java.awt.event.*;

public class ChatPanel extends JFrame{
	private JTextArea GM;
	private JTextArea PM;
	private JTextField TX;
	private JLabel INF;
	private JButton quit;
	public static String message;
	public static boolean sent = false;
	public static boolean online = true;
	
	public ChatPanel() {
		
		Action SendAction = new SendAction("SEND");
		Action lineAction = new lineAction("BUSY");
		
		GM = new JTextArea(10,30);
    	PM = new JTextArea(10,30);
    	TX = new JTextField(22);
    	INF = new JLabel();
    	JButton send = new JButton("SEND");
    	send.setAction(SendAction);
    	quit = new JButton("BUSY");
    	quit.setAction(lineAction);
    	
    	JPanel info = new JPanel(new FlowLayout(FlowLayout.LEFT));
    	info.setPreferredSize(new Dimension(350, 100));
    	info.setBorder(new TitledBorder("Info"));
    	JPanel MSG = new JPanel(new GridLayout(2,1));
    	JPanel gMessage = new JPanel();
    	gMessage.setBorder(new TitledBorder("Group Message"));
    	JPanel pMessage = new JPanel();
    	pMessage.setBorder(new TitledBorder("Personal Message"));
    	JPanel tMessage = new JPanel(new FlowLayout(FlowLayout.LEFT));
    	tMessage.setBorder(new TitledBorder("Type Message"));
    	tMessage.setPreferredSize(new Dimension(350, 60));
    	
    	Container ct = getContentPane();
        ct.setLayout(new FlowLayout());
    	
        ct.add(info, BorderLayout.NORTH);
        info.add(INF);
        ct.add(MSG, BorderLayout.NORTH);
        MSG.add( gMessage, BorderLayout.NORTH);
        gMessage.add(GM);
        MSG.add( pMessage, BorderLayout.NORTH);
        pMessage.add(PM);
        ct.add( tMessage, BorderLayout.NORTH);
        tMessage.add(TX);
        tMessage.add(send);
        ct.add(quit);
        
        this.getRootPane().setDefaultButton(send);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Face Up");
        setSize(400, 650);
        setVisible(true);
        
	}
	
	public void appendGM(String message){
		GM.append(message + "\n");
	}
	
	public void appendPM(String message){
		PM.append(message + "\n");
	}
	
	public void  updateINF(String info){
		INF.setText(info);
	}
	
	public class SendAction extends AbstractAction {
	      /** Constructor */
	      public SendAction(String name) {
	         super(name);
	         putValue(SHORT_DESCRIPTION, "send the message");
	      }
	 
	      @Override
	      public void actionPerformed(ActionEvent e) {
	         message = TX.getText();
	         TX.setText("");
	         sent = true;
	      }
	   }
	
	public class lineAction extends AbstractAction {
	      /** Constructor */
	      public lineAction(String name) {
	         super(name);
	         putValue(SHORT_DESCRIPTION, "Go Offline");
	      }
	 
	      @Override
	      public void actionPerformed(ActionEvent e) {
	    	  if (online){
	    		  quit.setText("GO ONLINE");
	    		  message = "/offline";
	    		  updateINF("you are now offline");
		    	  online = false;
	    	  }else{
	    		  quit.setText("BUSY");
	    		  message = "/online";
	    		  updateINF("you are now online");
	    		  online = true;
	    	  }
		      sent = true;
	    	
	      }
	   }
	
}